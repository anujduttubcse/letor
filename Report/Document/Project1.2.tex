\documentclass{article}
\usepackage[top=1.5in, left=1in, right=1in, bottom=1.5in]{geometry}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{biblatex}
\usepackage[english]{babel}

\title{Learning to Rank using Linear Regression}
\author{Anuj Dutt \\
  \texttt{anujdutt}\\
  \texttt{50292024}
  }
\begin{document}
  \maketitle
  \newpage
  \section{Overview}
  The goal of this project is to use machine learning to solve a problem that arises in Information Retrieval, one known as the Learning to Rank (LeToR) problem. We formulate this as a problem of linear regression where we map an input vector x to a real-valued scalar target \textbf{y(x, w)}.
  \newline
  \hspace*{5mm}There are two tasks:
  \newline
  \hspace*{5mm}1. Train a linear regression model on LeToR dataset using a closed-form solution.
  \hspace*{5mm}2. Train a linear regression model on the LeToR dataset using stochastic gradient descent (SGD).
  \newline
  \hspace*{5mm}The LeToR training data consists of pairs of input values x and target values t. The input values are real-valued vectors (features derived from a query-document pair). The target values are scalars (relevance labels) that take one of three values 0, 1, 2: the larger the relevance label, the better is the match between query and document. Although the training target values are discrete we use linear regression to obtain real values which is more useful for ranking (avoids collision into only three possible values).
  \subsection{Random variable}
  A random variable is a variable that can take on different values randomly. Each different occurrence of a random variable is associated with its probability of occurrences. A random variable may be discrete or continuous. A discrete random variable has a finite or countably infinite number of states. A continuous random variable has an infinite number of states.
  \begin{equation*}x = \{ x_1, x_2, x_3, ... , x_n \}\end{equation*}
  \hspace*{5mm}On its own, a random variable is just a description of the states that are possible. A \textbf{probability distribution} specifies the probabilities of occurrences of these states.
  \paragraph{Probability distribution function(PDF)}
   maps the likeliness of a random variable to take on a certain state to each of its possible states. The description of probability distribution function depends on whether the variables are discrete or continuous.
  \newline
  \hspace*{5mm}The PDF of a continuous random variable \textbf{X} is given by a function \textbf{f(x)} if the following conditions are met:
  \begin{equation*}\int_{-\infty}^{\infty}f(x) dx = 1\end{equation*}
  \begin{equation*}\forall x \in X,f(x) \geq 0\end{equation*}
  \paragraph{Expectation}
  of a random variable is the long run tendency of the repetition of experiments for that random variable. Let X be a continuous random variable with range [a, b] and probability density function f(x). The expected value of X is defined by:
  \begin{equation*}E(x) = \int_{a}^{b}xf(x) dx\end{equation*}
  \paragraph{Standard deviation}
  denoted by $\sigma $, is a measure of the spread or scale of the values of the random variable.
  \paragraph{Variance}
  of a random variable is the square of the standard deviation and for a continuous random variable X with range [a, b] and probability density function f(x), it is given by:
  \begin{equation*}\sigma^2 = \int_{a}^{b}(x - \mu)^2f(x) dx\end{equation*}
  \begin{equation*}\sigma^2 = \int_{a}^{b}(x - E(x))^2f(x) dx\end{equation*}
  Using some mathematical simplification we can simplify this expression to:
  \begin{equation*}\sigma^2 = E(x^2)- E(x)^2 dx\end{equation*}
  \paragraph{Covariance}
  is a measure of the relation between two random variables. They may exhibit a positive relationship with one another if, greater values of one random variable correspond with greater values of the other random variable. In this case, the covariance is positive. If greater values of one random variable correspond with lesser values of the other random variable they exhibit a negative relationship with one another. In this case, the covariance is negative. The mathematical representation of covariance between two random variables X and Y is given by:
  \begin{equation*}Cov(X,Y) = E[X - E[X]) (Y - E[Y])]\end{equation*}
  \paragraph{Covariance matrix}
  is a matrix which stores the values of covariances between \textbf{n} different random variables in an $n \times n$ matrix. The value at (i,j) of a covariance matrix is the covariance between the i\textsuperscript{th} random variable and the j\textsuperscript{th} random variable. The value at (i,i) are the variances of the random variables themselves as the covariance of a random vairable X with itself is its variance.
  \subsection{Normal (gaussian) distribution}
  The most commonly used distribution over real numbers is the normal distribution, also known as the Gaussian distribution:
  \begin{equation*}P(x) = \frac{1}{{\sigma \sqrt {2\pi } }}e^{{{ - \left( {x - \mu } \right)^2 } \mathord{\left/ {\vphantom {{ - \left( {x - \mu } \right)^2 } {2\sigma ^2 }}} \right. \kern-\nulldelimiterspace} {2\sigma ^2 }}}\end{equation*}

  The normal distribution exists in the space of $(-\infty, \infty)$. Its mean also lies in the range of $(-\infty, \infty)$. It has a positive standard deviation and by definition a positive variance.
  \newline
  \hspace*{5mm}If the standard deviation of a normal distribution is more, then the data is more spread out in the tail region and the height of the distribution is lesser. If the normal distribution has a mean of 0 and a standard deviation of 1, then its called a standard normal distribution.
  \newline
  \hspace*{5mm}The Multivariate normal distribution is a generalization of the univariate normal distribution which has the density function:
  \begin{equation*}P(x)=\frac{1}{\sqrt{(2\pi)^{n}|\Sigma|}}exp(-\frac{1}{2}(y_{i}-\mu)^T\Sigma^{-1}(y_{i}-\mu))\end{equation*}
  \newline
  \hspace*{5mm}In the above equation $\mu$ consists of the means of n components and $\Sigma$ is the covariance matrix of all the n components.
  \section{Linear regression}
  The goal of regression is to explore the relation between the input features with that of the target value and give us a continuous valued output for the given unknown data.
  \begin{equation*}y(x,w) = w_0 + w_1(x) \end{equation*}
  Where:
  \begin{itemize}
  \item $y(x,w)$ is the predicted label (a desired output).
  \item $w_0$ is the bias (the y-intercept).
  \item $w_1$ is the weight of feature 1. Weight is the same concept as the "slope".
  \item $x$ is a feature (a known input).
  \end{itemize}

  \hspace*{5mm}Although this model uses only one feature, a more sophisticated model might rely on multiple features, each having a separate weight. For example, a model that relies on three features might look as follows:
  \begin{equation*}y(x,w) = w_0 + w_1(x) + w_2(x) + w_3(x) \end{equation*}

  \paragraph{Loss}
  is the error due to a pad prediction. It denotes how ineffective our model was in predicting the output for a certain input. If our prediction is correct, then the loss is 0; otherwise the losee is higher. The goal of training should therefore be to find a set of weights and bias so that our resultant loss is minimum.

  \paragraph{Training}
  a model implies altering our weights and biases to ensure that our loss in minimised. This may be done in many ways. One of the most popular approachs is gradient descent. We shall be employing gradient descent in one of our approaches.

  \paragraph{Mean square error (MSE)}
  is the mean of the sum of the squares of the errors of each training output with respect to the expected output.

  \subsection{Multivariate linear regression using basis functions}
  The model discussed above is a linear function of the parameters $w_0 , w_1, w_2 . . , w_M$. It is also, however, a linear function of the input variables $x_i$. This imposes \textbf{significant limitations} on the model as we may have non-linear relationships between our input and the output. We extend our model by considering linear combinations of fixed non-linear functions of the input vector. These non-linear functions are called as \textbf{basis functions}.
  \newline
  \hspace*{5mm}The basis functions are responsible for introducing \textbf{non linearity} in our system. The model is still a linear function of the parameters, which gives it simple analytical properties, but it is now non-linear with respect to the input vector. The new model for an input vector of M-1 components may now be represented as:\begin{equation*}y(\textbf{x,w}) = w_0 + \sum_{j=1}^{M-1}w_j\phi_j(x) \end{equation*}

  \hspace*{5mm}The parameter $w_0$ allows for any fixed offset in the data and is sometimes called a \textbf{bias parameter}. The mapping from parameters to predictions is still a linear function but the mapping from features to predictions is now an affine function. This extension to affine functions means that the plot of the model’s predictions still looks like a line, but it need not pass through the origin. It is often convenient to define an additional dummy 'basis function' $\phi_0(x) = 1$ so that: \begin{equation*}y(\textbf{x,w}) = \sum_{j=0}^{M-1}w_j\phi_j(x) \end{equation*}

  \hspace*{5mm}Now consider a data set of inputs $\textbf{X} = \{ x_1, x _2, . . . , x_N \}$ ,where $x_i$ is a vector of \textbf{M} components, with corresponding target values $\textbf{T} = \{ t_1. t_2, . . . , t_N \}$. We group the target variables $\{t_n\}$ into a column vector that we denote by \textbf{T}. Let \textbf{\^{T}} be the value that our model predicts $\textbf{T}$ should take on. The resulting equation is:\begin{equation*}\textbf{\^{T}} = \bm{\Phi(X)} \textbf{w}\end{equation*}

  \hspace*{5mm}Parameters are values that control the behavior of the system. In this case, $w_i$ is the coefficient that we multiply by $\phi_i(x)$ before summing up the contributions from all the $\phi(x)$. We can think of w as a set of weights that determine how each feature affects the prediction. If $\phi_i(x)$ receives a positive weight $w_i$,then increasing the value of $\phi_i(x)$ increases the value of our prediction $\mathbf{\hat{t}_i}$. If a feature receives a negative weight, then increasing the value of $\phi_i(x)$ decreases the value of our prediction $\mathbf{\hat{t}_i}$. If a $\phi_i(x)$’s weight is large in magnitude, then it has a large effect on the prediction. If a $\phi_i(x)$’s weight is zero, it has no effect on the prediction.

  \hspace*{5mm}One way of measuring the performance of the model is to compute the \textbf{mean squared error} of the model on the test set. If $\mathbf{\hat{T}^(test)}$  gives the predictions of the model on the test set, then the mean squared error is given by:\begin{equation*}MSE_{test} = \dfrac{1}{N}\sum_{i=1}^{N}(\textbf{\^{t}}^{(test)} - \textbf{t}^{(test)})^2_i\end{equation*}
  \subsection{Capacity, Overfitting and Underfitting}
  The central challenge in machine learning is that we must perform well on \textbf{new, previously unseen inputs}—not just those on which our model was trained. The ability to perform well on previously unobserved inputs is called \textbf{generalization}.
  \newline
  \hspace*{5mm}Typically, in a machine learning algorithm, we have access to a training set. We compute some error measure on the training set called the training error, and we reduce this training error. The \textbf{generalization error} is defined as the expected value of the error on a new input.
  \newline
  \hspace*{5mm}We expect the training error of a randomly selected model to equal the expected test error of that model. However, during the training, we choose the set of parameters to reduce training set error, then sample the test set. Under this process, the expected test error is greater than or equal to the expected value of training error. The factors determining how well a machine learning algorithm will perform are its ability to:
  \begin{enumerate}
  \item Make the training error small.
  \item Make the gap between training and test error small.
  \end{enumerate}
  \hspace*{5mm}These two factors correspond to the two central challenges in machine learning: \textbf{underfitting} and \textbf{overfitting}. Underfitting occurs when the model is not able to obtain a sufficiently low error value on the training set. Overfitting occurs when the gap between the training error and test error is too large.
  We can control whether a model is more likely to overfit or underfit by altering its \textbf{capacity}. Informally, a model’s capacity is its ability to fit a wide variety of functions.
  One way to control the capacity of a learning algorithm is by choosing its hypothesis space, the set of functions that the learning algorithm is allowed to select as being the solution.Models with insufficient capacity are unable to solve complex tasks. Models with high capacity can solve complex tasks, but when their capacity is higher than needed to solve the present task they may overfit.

  \begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{40mm}
      \includegraphics[width=\linewidth]{../Images/Underfit.png}
      \caption{Underfit}
    \end{subfigure}
    \begin{subfigure}[b]{40mm}
      \includegraphics[width=\linewidth]{../Images/Fit.png}
      \caption{Appropriate capacity}
    \end{subfigure}
    \begin{subfigure}[b]{40mm}
      \includegraphics[width=\linewidth]{../Images/Overfit.png}
      \caption{Overfit}
    \end{subfigure}
    \caption{The cases of underfit, appropriate fit and overfit}
    \label{fitting}
  \end{figure}

  Figure \ref{fitting} shows this principle in action. We take the a predictor system as:
  \begin{equation*}t = \sum_{j=0}^{M}w_j\phi_j(x)  \textnormal{ where } \phi_j(x) = x^j\textnormal{ ; $w_0 = bias$ }\end{equation*}

  We compare a linear, quadratic and degree-9 predictor attempting to fit a problem where the true underlying function is quadratic. The linear function is unable to capture the curvature in the true underlying problem, so it underfits. The degree-9 predictor is capable of representing the correct function, but it is also capable of representing infinitely many other functions that pass exactly through the training points, because we have more parameters than training examples. We have little chance of choosing a solution that generalizes well when so many wildly different solutions exist. In this example, the quadratic model is perfectly matched to the true structure of the task so it generalizes well to new data.

  \subsection{Regularization}
  Overfitting generally happens when:
  \begin{enumerate}
  \item The number of parameters is too high w.r.t the number of inputs.
  \item The number of inputs to be trained upon is too small w.rt. the number of parameters.
  \end{enumerate}

  When overfitting occurs we observe large values for the parameters which tend to mimic our input data it is being trained upon. This leads to large values in \textbf{generalization error}. Let us take the example of an overfitted model given by the equation:
  \begin{equation*}t(x) = w_0 \phi_0{(x)} + w_1 \phi_1{(x)} + w_2 \phi_2{(x)} + w_3 \phi_3{(x)}+ w_4 \phi_4{(x)}\end{equation*}
  \hspace*{5mm} Our goal is to minimise the error function for this equation:
  \begin{equation*}\min_{w} \dfrac{1}{N}\sum_{i=1}^{N}(t(x) - t)^2_i\end{equation*}
   \hspace*{5mm}This quantity is minimum when the effect of the large valued parameters is minimized. For simplicity let us try to minimise the weight of $w_3$ and $w_4$. We can do this by introducing the square of $w_3$ and $w_4$ multiplied by a constant into our error function:
  \begin{equation*}\min_{w} \dfrac{1}{N}\left(\sum_{i=1}^{N} (t(x) - t)^2_i + 10000w^2_3 + 10000w^2_4\right) \end{equation*}
  \hspace*{5mm} Now we can say that the error function can be minimised only if $w^2_3$ and $w^2_3$ take very small values. Thus we have minimised the effect of large values of $w_3$ and $w_3$ in our model, thereby preventing overfitting of the model. This is called as adding a \textbf{weight decay} for $w_3$ and $w_3$ in our model.
  In a general case we now use the following equation for our error function:
  \begin{equation*}\dfrac{1}{N}\left(\sum_{i=1}^{N}(t(x) - t)^2_i + \sum_{j=0}^{M}\lambda w^2_j\right) \textnormal{ where $\lambda$ is a constant}\end{equation*}

  \hspace*{5mm}We vary the amount of weight decay to prevent these high-degree models from overfitting.With very large $\lambda$, we can force the model to learn a function with no slope at all. This underfits because it can only represent a constant function. With a medium value of $\lambda$, the learning algorithm recovers a curve with the right general shape. Even though the model is capable of representing functions with much more complicated shape, weight decay has encouraged it to use a simpler function described by smaller coefficients.

  \subsection{Clustering}
  Clustering is the task of dividing the population or data points into a number of groups such that data points in the same groups are more similar to other data points in the same group than those in other groups. In simple words, the aim is to to find homogeneous subgroups among the observations and assign them into different clusters. The simplest algorithm for finding clusters is:

  \paragraph{k-means}
  is one of the simplest unsupervised learning algorithms that solve the clustering problems. The procedure follows a simple and easy way to classify a given data set through a certain number of clusters (assume k clusters). The main idea is to define \textbf{k} centers, one for each cluster. The algorithm is as follows:
  \begin{enumerate}
  \item \textbf{Cluster assignment}: The algorithm goes through each of the data points and depending on which cluster is closer, it assigns the data points to one of the three cluster centroids.
  \item \textbf{Move centroid}: Here, k-means moves the centroids to the average of the points in a cluster. In other words, the algorithm calculates the average of all the points in a cluster and moves the centroid to that average location.
  \end{enumerate}
  This process is repeated until there is no change in the clusters (or possibly until some other stopping condition is met). \textbf{k} is chosen randomly or by giving specific initial starting points by the user.

  Clustering is an unsupervised machine learning approach, but can it be used to improve the accuracy of supervised machine learning algorithms as well by clustering the data points into similar groups and using these cluster labels as independent variables in the supervised machine learning algorithm.

  \section{Closed form solution}
  \hspace*{5mm}To make a machine learning algorithm, we need to design an algorithm that will improve the weights \textbf{w} in a way that reduces $MSE_{test}$ when the algorithm is allowed to gain experience by observing a training set $(x^{(train)},t^{(train)})$. One intuitive way of doing this is just to minimize the mean squared error on the training set, $MSE_{(train)}$.
  \newline
  \hspace*{5mm}To minimize $MSE_{(train)}$, we can simply solve for where its gradient is \textbf{0}:
  \begin{equation*}\nabla_w(MSE_{(train)}) = 0\end{equation*}
  \begin{equation*}\nabla_w(\dfrac{1}{N}\sum_{i=1}^{N}(\textbf{\^{t}}^{(train)} - \textbf{t}^{(train)})^2_i) = 0\end{equation*}\\
  \begin{equation*}\nabla_w(\dfrac{1}{N}(\textbf{\^{T}}^{(train)} - \textbf{T}^{(train)})^2) = 0\end{equation*}\\
  \begin{equation*}\nabla_w(\dfrac{1}{N}(\bm{\Phi}^{(train)} \textbf{w} - \textbf{T}^{(train)})^2) = 0\end{equation*}\\
  \begin{equation*}\nabla_w((\bm{\Phi}^{(train)} \textbf{w} - \textbf{T}^{(train)})^2) = 0\end{equation*}\\
  \begin{equation*}\nabla_w((\bm{\Phi}^{(train)} \textbf{w} - \textbf{T}^{(train)})^\textbf{T} (\bm{\Phi}^{(train)} \textbf{w} - \textbf{T}^{(train)})) = 0\end{equation*}\\
  \begin{equation*}\nabla_w(((\bm{\Phi}^{(train)})^2 \textbf{w}^2 - 2 \bm{\Phi}^{(train)} \textbf{T}^{(train)}\textbf{w} + (\textbf{T}^{(train)})^2) = 0\end{equation*}\\
  \begin{equation*}(\bm{\Phi}^{(train)})^2 \textbf{w} = \bm{\Phi}^{(train)} \textbf{T}^{(train)}\end{equation*}\\
  \begin{equation*}(\bm{\Phi}^{(train)})^T \bm{\Phi}^{(train)} \textbf{w} = \bm{\Phi}^{(train)} \textbf{T}^{(train)}\end{equation*}\\
  \hspace*{5mm}Premultiplying by $((\bm{\Phi}^{(train)})^T \bm{\Phi}^{(train)})^{-1}$:
  \begin{equation*}\textbf{w} = ((\bm{\Phi}^{(train)})^T \bm{\Phi}^{(train)})^{-1} \bm{\Phi}^{(train)} \textbf{T}^{(train)}\end{equation*}
  \hspace*{5mm}The term $((\bm{\Phi}^{(train)})^T \bm{\Phi}^{(train)})^{-1} \bm{\Phi}^{(train)}$ is the \textbf{Moore-Penrose pseudo-inverse} of the matrix $\bm{\Phi}$.

  \hspace*{5mm}The system of equations whose solution is given by the above equation is known as the \textbf{normal equations}. Evaluating it constitutes a simple learning algorithm.
  \newline
  \hspace*{5mm}It is worth noting that our model contains the bias parameter, $w_0$ and a corresponding basis function $\phi_0(x) = 1$ which ensures that our model doesnt have to pass through the origin. It gives us more flexibility with our predictions. In other words, output of the transformation is \textbf{biased} toward being $w_0$ in the absence of any input.
  \section{Stochastic gradient descent}
  For a given input value, when we have the error distance from the predicted output, the training process can now iterate over the input values, compute this error distance, and try to minimize this error distance. One method to do this is the \textbf{gradient descent method}.
  The stochastic gradient descent algorithm first takes a random initial value $w^{(0)}$. Then it updates the value of $w$ using
  \begin{equation*}w^{(\tau + 1)} = w^{\tau} + \delta w^{\tau}\end{equation*}
  where $\delta w^{\tau} = -\eta^{\tau}\nabla_wE$ is called the \textbf{weight updates}. It goes along the opposite direction of the gradient of the error. $-\eta^{\tau}$ is the \textbf{learning rate}, deciding how big each update step would be. Because of the linearity of differentiation, we have:
  \begin{equation*}E = \dfrac{1}{N}\left(\sum_{i=1}^{N}(t(x) - t)^2_i + \sum_{j=0}^{M}\lambda w^2_j\right) \textnormal{ where $\lambda$ is a constant}\end{equation*}
  \begin{equation*}\nabla_w E = \nabla_w(\dfrac{1}{2N}\sum_{i=1}^{N}(\textbf{\^{t}}^{(train)} - \textbf{t}^{(train)})^2_i + \dfrac{\lambda}{2N}\sum_{j=0}^{M}w_j^2)\end{equation*}\\
  \begin{equation*}\nabla_w E = \nabla_w(\dfrac{1}{2N}\sum_{i=1}^{N}(\sum_{j=0}^{M}t(w_j,\phi_j(x_i)) - \textbf{t}^{(train)})^2_i + \dfrac{\lambda}{2N}\sum_{j=0}^{M}w_j^2)\end{equation*}\\
  \begin{equation*}\nabla_w E = \dfrac{1}{N}\sum_{i=1}^{N}(\sum_{j=0}^{M}t(w_j,\phi_j(x_i)) - \textbf{t}^{(train)})_i \phi_j(x_i) + w^{\tau}\end{equation*}\\

  For a given vector x (of 3 dimensions) the gradient vector is a [3,1] matrix with each element representing the partial derivative of that vector in a dimension.Thus, the gradient vector tells us how significant a slight variation in our input in any one dimension is for the output. Thus the gradient vector in the direction of the input weight vector gives us the direction of slope. By iteratively making these changes to the weights and measuring the new loss, we eventually arrive at a certain weight vector where out loss function is very close to zero. This is our desired model.
  \paragraph{Learning rate} While iteratively making changes to the weights over many epochs does give us an output eventually, we generally scale the change by a factor called the learning rate. This learning rate determines how fast we approach the point of minimum loss.
  If the learning rate is too less, the network will take a longer time to train, if the learning rate is too high then the network may miss the point of minimum loss as the change applied to the weights may be too large.

  \section{Observations}
  We run both the solutions using many different hyper parameters. The following is a documentation of the observations made by varying the hyper parameters.
  \subsection{Closed form solution}
  For the closed form solution we vary the 3 hyperparamaters, namely: cluster size, $\lambda$, variance of $\Sigma$.
  \subsubsection{Number of clusters}We change the number of clusters and observe values for the RMS error, and the accuracy for training, testing and validation cases.
  \begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
      \includegraphics[width=\linewidth]{../Images/RMS_2.png}
      \caption{RMS error}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
      \includegraphics[width=\linewidth]{../Images/Accuracy_2.png}
      \caption{Accuracy}
    \end{subfigure}
    \caption{The RMS error and accuracy for training, validation and testing for various cluster sizes}
    \label{clusters_1}
  \end{figure}
  We observe that as we increase the number of clusters the RMS error falls rapidly from cluster size of 1 to 5 but then starts to plateau. The accuracy for testing has a stable fall as we increase the cluster size. We now choose cluster size 5 as our cluster choice for subsequent experiments. We expect the accuracy to drop even further if we increase our cluster size to a very high value. In this case we may start to overfit the data. We normally choose the best value for clusters by emplyong cross-validation.
  \subsubsection{Lambda}
  \begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.45\linewidth}
      \includegraphics[width=\linewidth]{../Images/Lambda_RMS_1.png}
      \caption{RMS error}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
      \includegraphics[width=\linewidth]{../Images/Lambda_Accuracy_1.png}
      \caption{Accuracy}
    \end{subfigure}
    \caption{The RMS error and accuracy for training, validation and testing for various Lambda values}
    \label{lambda_1}
  \end{figure}
  We observe that as we increase the size of lambda the RMS error falls rapidly from cluster size of 1 to 5 but then starts to plateau. We have choosen a cluster size of 200 for our experiment. We expect the accuracy to drop if we increase our cluster size to a very high value. In this case we may start to overfit the data. We normally choose the best value for clusters by emplyong cross-validation.
  \subsubsection{Variance}
  \begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.4\linewidth}
      \includegraphics[width=\linewidth]{../Images/Variance_RMS_2.png}
      \caption{RMS error}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\linewidth}
      \includegraphics[width=\linewidth]{../Images/Variance_Accuracy_2.png}
      \caption{Accuracy}
    \end{subfigure}
    \caption{The RMS error and accuracy for training, validation and testing for various variance values}
    \label{variance_1}
  \end{figure}
  We observe that as we lower the size of the variance to below 0.1 times the original variance the accuracy drops. After that the accuracy plateaus. If it is small, the small changes in distance will have large effect. If it is large, the small changes in distance will have low effect with increasing distance from center is slow). The optimal value should be looked for (it is usually found with cross-validation).
  \subsection{Gradient descent solution}
  For the gradient descent solution we vary the 4 hyperparamaters, namely: cluster size, $\lambda$ and learning rate, variance of $\Sigma$.
  \subsubsection{Number of clusters}We change the number of clusters and observe values for the RMS error, and the accuracy for training, testing and validation cases.
  \begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.35\linewidth}
      \includegraphics[width=\linewidth]{../Images/SGD_Cluster_RMS.png}
      \caption{RMS error}
    \end{subfigure}
    \begin{subfigure}[b]{0.35\linewidth}
      \includegraphics[width=\linewidth]{../Images/SGD_Cluster_Accuracy.png}
      \caption{Accuracy}
    \end{subfigure}
    \caption{The RMS error and accuracy for training, validation and testing for various cluster sizes}
    \label{clusters_2}
  \end{figure}
  We observe that as we increase the number of clusters, initially the accuracy stays constant but as the values approach 60 we start to see a constant decrease in the accuracy of the system. We expect the accuracy to drop even further if we increase our cluster size to a very high value. We normally choose the best value for clusters by emplyong cross-validation.
  \subsubsection{Lambda}
  \begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.35\linewidth}
      \includegraphics[width=\linewidth]{../Images/SGD_Lambda_RMS.png}
      \caption{RMS error}
    \end{subfigure}
    \begin{subfigure}[b]{0.35\linewidth}
      \includegraphics[width=\linewidth]{../Images/SGD_Lambda_Accuracy.png}
      \caption{Accuracy}
    \end{subfigure}
    \caption{The RMS error and accuracy for training, validation and testing for various Lambda values}
    \label{lambda_2}
  \end{figure}
  We observe that as we increase the size of $\lambda$ the RMS error stays fairly constant for our cluster size of 10. We expect a very low $\lambda$ to lower accuracy if we have many basis functions in our model. We normally choose the best value for clusters by emplyong cross-validation.
  \subsubsection{Learning rate}
  \begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.35\linewidth}
      \includegraphics[width=\linewidth]{../Images/SGD_Learning_rate_RMS.png}
      \caption{RMS error}
    \end{subfigure}
    \begin{subfigure}[b]{0.35\linewidth}
      \includegraphics[width=\linewidth]{../Images/SGD_Learning_rate_Accuracy.png}
      \caption{Accuracy}
    \end{subfigure}
    \caption{The RMS error and accuracy for training, validation and testing for various learning rate values}
    \label{learning_rate_2}
  \end{figure}
  We observe that as we increase the size of $\eta$ the RMS error stays fairly constant for our cluster size of 10. We expect a very low $\eta$ to lower accuracy for the same number of iterations as we not reach the optimal values for the weights. This is actually observed in our graph as we have higher RMS errors for very low values of $\eta$.
  \subsubsection{Variance}
  \begin{figure}[h!]
    \centering
    \begin{subfigure}[b]{0.3\linewidth}
      \includegraphics[width=\linewidth]{../Images/SGD_Variance_RMS_2.png}
      \caption{RMS error}
    \end{subfigure}
    \begin{subfigure}[b]{0.3\linewidth}
      \includegraphics[width=\linewidth]{../Images/SGD_Variance_Accuracy_2.png}
      \caption{Accuracy}
    \end{subfigure}
    \caption{RMS error and accuracy for training, validation and testing for various variances}
    \label{variance_2}
  \end{figure}
  We observe that as we lower the size of the variance to below 0.1 times the original varaince the accuracy drops. After that the accuracy increases till we reach a multiplier value of 1 and then plateaus. If it is small, the small changes in distance will have large effect. If it is large, the small changes in distance will have low effect with increasing distance from center is slow). The optimal value should be looked for (it is usually found with cross-validation).
\section{Conclusion}
We can now make some conclusions about linear regression basics.
\begin{enumerate}
  \item There are two simple forms for solving linear regression problems: Closed Form Solution, Gradient Descent Solution.
  \item The input samples can be split into various clusters using k means clustering. We use these clusters to position our basius functions.
  \item The regularizer term is added to prevent overfitting by introducting a weight decay to the system.
  \item The variance of the the Gaussian radial function helps to control its spread and influence.
  \item In SGD, a higher learning rate means we may overshoot the local minima. Whereas a low learning rate means it takes longer to reach this local minima.
\end{enumerate}
\begin{thebibliography}{9}
\bibitem{nano3}
Deep Learning Ian Goodfellow Yoshua Bengio Aaron Courville
\bibitem{nano3}
Pattern Recognition and Machine Learning - Christopher M. Bishop
\bibitem{nano3}
https://towardsdatascience.com
\bibitem{nano3}
https://medium.com/data-science-group-iitr
\bibitem{nano3}
https://chemicalstatistician.wordpress.com
\end{thebibliography}
\end{document}
