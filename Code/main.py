import pandas as pd
import numpy as np
import math
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt


# We read from the input data file by creating a pandas DataFrame.
# The returning matrix is of the size (Number of inputs x Number of features in each input).
# In our case it should be (69623, 46).
def generate_raw_data_input_values(file_path):
    raw_data_frame = pd.read_csv(file_path, header=None)
    return raw_data_frame.values


# We need to find out the number of relevant features from our data. A "relevant" feature corresponds to a
# feature with non zero variance. If the variance of a feature column is 0, we then proceed to drop that column.
# We do this by using the np.var() method and passing the axis as the row(We want to compute the feature across the
# rows for each column). We now get an array with the variance of each feature in an array.
# We drop columns in the raw data matrix whose index equals to 0 values in the variance array.
# We then proceed to drop that entry from the variance array.
# We now return the altered raw_data and the number of features.
# We do this processing only if is_synthetic is True. If false, we just return the raw_data and the number
# of features directly.
def generate_raw_data_with_relevant_features(raw_data, is_synthetic):
    variance_matrix = np.var(raw_data, axis=0)
    if is_synthetic is False:
        zero_variance_index = np.where(variance_matrix == 0)[0]
        raw_data = np.delete(raw_data, zero_variance_index, axis=1)
        variance_matrix = np.delete(variance_matrix, zero_variance_index)
    return raw_data, len(variance_matrix)


# We read from the output data file by creating a pandas DataFrame. The returning array is of the size 69623.
def generate_raw_data_output_values(file_path):
    target_data_frame = pd.read_csv(file_path, header=None, usecols=[0])
    return target_data_frame.values


# We now split the data into training, validation and testing components.

# Get training input values. We slice the array from [0, 80% of length of array].
def generate_training_input_vectors(raw_training, training_percent=80):
    training_len = int(math.ceil(len(raw_training) * (training_percent / 100)))
    return np.array(raw_training[0:training_len, :])


# Get training output values. We slice the array from [0, 80% of length of array].
def generate_training_output_values(raw_training, training_percent=80):
    training_len = int(math.ceil(len(raw_training) * (training_percent / 100)))
    return np.array(raw_training[0: training_len])


# Get validation input values. We slice the array from [80% of length of array, 90% of length of array].
def generate_validation_input_vectors(raw_training, validation_percent, skip_count):
    validation_len = int(math.ceil(len(raw_training) * (validation_percent / 100)))
    validation_end_index = skip_count + validation_len
    return np.array(raw_training[skip_count + 1:validation_end_index, :])


# Get validation output values. We slice the array from [80% of length of array, 90% of length of array].
def generate_validation_output_values(raw_training, validation_percent, skip_count):
    validation_len = int(math.ceil(len(raw_training) * (validation_percent / 100)))
    validation_end_index = skip_count + validation_len
    return np.array(raw_training[skip_count + 1:validation_end_index])


# Generate the BigSigma matrix. This is a covariance matrix.
# A covariance matrix stores the covariances of many Random variables in it. While noting the covariances of
# N random variables, it has a size of (N x N). The value at (i,j) is the covariance between the ith and jth
# random variable. The diagonal elements (i,i) thus denote a random variable's covariance with itself.
# This is actually the variance of the random variable. The project report goes into the covariance matrix in detail.
# Here we store the variances of the features. All the covariances are assumed to be zero. We have assumed that all
# features are independent random variables.
def generate_big_sigma_matrix(data, number_of_features, is_synthetic):
    big_sigma_matrix = np.zeros(shape=(number_of_features, number_of_features))
    variance_matrix = np.var(data, axis=0)
    for feature in range(number_of_features):
        big_sigma_matrix[feature][feature] = variance_matrix[feature]
    if is_synthetic:
        big_sigma_matrix = np.dot(3, big_sigma_matrix)
    else:
        big_sigma_matrix = np.dot(200, big_sigma_matrix)
    return big_sigma_matrix


# Get the phi matrix for the input provided.
# The number of rows of the phi matrix is equal to the number of inputs and the number of columns represent
# the number of basis functions. In our case the number of basis functions are 10.
def generate_phi_matrix_for_inputs(input_values, mu, big_sigma_inverse):
    phi_matrix = np.zeros((len(input_values), len(mu)))
    for feature in range(0, len(mu)):
        for input_index in range(0, len(input_values)):
            phi_matrix[input_index][feature] = compute_multi_variate_normal_distribution_value(
                input_values[input_index], mu[feature], big_sigma_inverse)
    return phi_matrix


# input_vector is a matrix of size (1x41)
# mu_vector is a matrix of size (1x41)
# big_sigma is a matrix of size (41x41)
# Simple implementation of a multivariate normal distribution function with mean values computed by the KMeans
# clustering algorithm.
def compute_multi_variate_normal_distribution_value(input_vector, mu_vector, big_sigma_inverse):
    return np.exp(-0.5 * np.dot(np.dot(np.subtract(input_vector, mu_vector),
                                       big_sigma_inverse), np.transpose(np.subtract(input_vector, mu_vector))))


# Output matrix for n inputs is of the size (n x 1). It is calculated from the dot product of the phi matrix(n x m)
# with the weight matrix (m x 1). Thus the weight matrix can be computed by
# pre-multiplying by the inverse of the phi matrix on both sides of the equation.
def compute_weight_matrix(output_matrix, phi_matrix, phi_matrix_transpose, c_lambda_diagonal_matrix):
    print("Shape of lambda matrix: {0}".format(np.shape(c_lambda_diagonal_matrix)))
    print("Shape of phi matrix: {0}".format(np.shape(phi_matrix)))
    return np.dot(np.dot(np.linalg.inv(
        np.add(c_lambda_diagonal_matrix, np.dot(phi_matrix_transpose, phi_matrix))),
        phi_matrix_transpose), output_matrix)


# Output matrix for n inputs is of the size (n x 1). It is calculated from the dot product of the phi matrix(n x m)
# with the weight matrix (m x 1).
def compute_output(weight_matrix, phi_matrix):
    return np.dot(phi_matrix, weight_matrix)


def compute_rms_error(computed_output, actual_output):
    total_error = 0
    counter = 0
    for i in range(0, len(computed_output)):
        total_error += math.pow(computed_output[i] - actual_output[i], 2)
        if int(np.around(computed_output[i], 0)) == actual_output[i]:
            counter += 1
    accuracy = (float((counter * 100)) / float(len(computed_output)))
    return "Accuracy: {0}, RMS error: {1}".format(accuracy, math.sqrt(total_error / len(computed_output)))


def compute_rms_error_and_accuracy(computed_output, actual_output):
    total_error = 0
    counter = 0
    for i in range(0, len(computed_output)):
        total_error += math.pow(computed_output[i] - actual_output[i], 2)
        if int(np.around(computed_output[i], 0)) == actual_output[i]:
            counter += 1
    accuracy = (float((counter * 100)) / float(len(computed_output)))
    return math.sqrt(total_error / len(computed_output)), accuracy


IsSynthetic = False
TrainingPercent = 80
ValidationPercent = 10
TestPercent = 10
NumberOfClusters = 10  # The number of clusters to be created.
C_Lambda = 0.03

BATCH_SIZE = 400
NUMBER_OF_BASIS_FUNCTIONS = NumberOfClusters
NUMBER_OF_ITERATIONS = 500

C_Lambda_Diagonal_Matrix = np.diag(np.diag(np.full((NumberOfClusters, NumberOfClusters), C_Lambda)))

RawData = generate_raw_data_input_values('Querylevelnorm_X.csv')
RawTarget = generate_raw_data_output_values('Querylevelnorm_t.csv')

ProcessedRawData, NumberOfFeatures = generate_raw_data_with_relevant_features(RawData, IsSynthetic)

TrainingData = generate_training_input_vectors(ProcessedRawData, TrainingPercent)
TrainingTarget = generate_training_output_values(RawTarget, TrainingPercent)
print("Training input shape: {0}".format(TrainingData.shape))
print("Training output shape: {0}".format(TrainingTarget.shape))

ValData = generate_validation_input_vectors(ProcessedRawData, ValidationPercent, (len(TrainingTarget)))
ValDataAct = generate_validation_output_values(RawTarget, ValidationPercent, (len(TrainingTarget)))
print("Validation input shape: {0}".format(ValData.shape))
print("Validation output shape: {0}".format(ValDataAct.shape))

TestData = generate_validation_input_vectors(ProcessedRawData, TestPercent, (len(TrainingTarget) + len(ValDataAct)))
TestDataAct = generate_validation_output_values(RawTarget, TestPercent, (len(TrainingTarget) + len(ValDataAct)))
print("Test input shape: {0}".format(TestData.shape))
print("Test output shape: {0}".format(TestData.shape))


def compute_sgd_output(input_array, weights_array):
    return np.matmul(input_array, weights_array)


def compute_output_for_i_input(input_array, weights_array, i):
    return np.matmul(input_array[i], weights_array.transpose())


# Computes the gradient based on the formula. Straightforward implementation.
def compute_gradient(input_array, start_index, end_index, output_array, weights_array, number_of_basis_functions,
                     lambda_term):
    gradient_vector = np.zeros((1, number_of_basis_functions))
    for basis_function_index in range(number_of_basis_functions):
        gradient = 0
        for input_index in range(start_index, end_index):
            computed_output = compute_output_for_i_input(input_array, weights_array, input_index)
            gradient += (computed_output - output_array[input_index]) * input_array[input_index][basis_function_index]
        gradient += lambda_term * weights_array[0][basis_function_index]
        gradient = gradient / (len(input_array))
        gradient_vector[0][basis_function_index] = gradient
    return gradient_vector


# Updates weights based on the gradient that is passed into it.
def update_weights(weights_array, gradient_vector, learning_rate):
    return np.subtract(weights_array, np.dot(gradient_vector, learning_rate))


def calculate_sum_of_differences(input_array, array_output, weight_array):
    np.subtract(np.matmul(input_array, weight_array), array_output)


def generate_closed_form_solution():
    print("Computing clusters...")
    # We divide the input feature space into "NumberOfClusters" clusters. With "NumberOfFeatures" features,
    # we end up with a matrix of shape ("NumberOfClusters", "NumberOfFeatures").
    # This means that we now have  with "NumberOfClusters" centroid values for the "NumberOfClusters" clusters
    # of the "NumberOfFeatures" features.
    kMeans = KMeans(n_clusters=NumberOfClusters, random_state=0).fit(TrainingData)
    Mu = kMeans.cluster_centers_
    print("Computed clusters. Now computing BigSigma...")
    BigSigma = generate_big_sigma_matrix(TrainingData, NumberOfFeatures, IsSynthetic)
    print("Computed BigSigma. Now computing PhiMatrixForTrainingData...")
    PhiMatrixForTrainingData = generate_phi_matrix_for_inputs(TrainingData, Mu, np.linalg.inv(BigSigma))
    print("Computed PhiMatrixForTrainingData. Now computing WeightMatrix...")
    WeightMatrix = compute_weight_matrix(TrainingTarget, PhiMatrixForTrainingData,
                                         np.transpose(PhiMatrixForTrainingData), C_Lambda_Diagonal_Matrix)

    TrainingOutput = compute_output(WeightMatrix, PhiMatrixForTrainingData)
    ValidationOutput = compute_output(WeightMatrix,
                                      generate_phi_matrix_for_inputs(ValData, Mu, np.linalg.inv(BigSigma)))
    TestOutput = compute_output(WeightMatrix, generate_phi_matrix_for_inputs(TestData, Mu, np.linalg.inv(BigSigma)))

    print('UBITname      = anujdutt')
    print('Person Number = 50292024')
    print('----------------------------------------------------')
    print("------------------LeToR Data------------------------")
    print('----------------------------------------------------')
    print("-------Closed Form with Radial Basis Function-------")
    print('----------------------------------------------------')
    print("M = {0} \nLambda = {1}".format(NumberOfClusters, C_Lambda))
    print("For training   = " + compute_rms_error(TrainingOutput, TrainingTarget))
    print("For validation = " + compute_rms_error(ValidationOutput, ValDataAct))
    print("For testing    = " + compute_rms_error(TestOutput, TestDataAct))


# A simple implementation of a gradient descent solution. Might not be as efficient as given in project code.
def generate_sgd_solution(training_input, training_target, validation_input, validation_target,
                          testing_input, testing_target, number_of_features, is_synthetic):

    print('----------Gradient Descent Solution--------------------')
    learning_rate = 1
    lambda_term = 2
    number_of_clusters = 10
    weight_array = np.ones((1, number_of_clusters))
    print("Computing cluster of size: {0}".format(number_of_clusters))
    kMeans = KMeans(n_clusters=number_of_clusters, random_state=0).fit(TrainingData)
    mu_matrix = kMeans.cluster_centers_
    print("Computed cluster of size: {0}".format(number_of_clusters))
    big_sigma_matrix = generate_big_sigma_matrix(training_input, number_of_features, is_synthetic)
    big_sigma_inverse = np.linalg.inv(big_sigma_matrix)
    phi_matrix_training_data = generate_phi_matrix_for_inputs(training_input, mu_matrix, big_sigma_inverse)
    phi_matrix_validation_data = generate_phi_matrix_for_inputs(validation_input, mu_matrix, big_sigma_inverse)
    phi_matrix_testing_data = generate_phi_matrix_for_inputs(testing_input, mu_matrix, big_sigma_inverse)
    start_index = 0
    for i in range(NUMBER_OF_ITERATIONS):
        start_index = start_index + BATCH_SIZE
        end_index = start_index + BATCH_SIZE
        if end_index > len(training_input):
            start_index = 0
            end_index = BATCH_SIZE

        print("ITERATION: {0}, START_INDEX_OF_BATCH:{1}, END_INDEX_OF_BATCH:{2}".format(i, start_index, end_index))
        weight_array = update_weights(weight_array,
                                      compute_gradient(phi_matrix_training_data, start_index, end_index, TrainingTarget,
                                                       weight_array, number_of_clusters, lambda_term), learning_rate)
    print("Computed weight_matrix.")
    print("Computing training_output...")
    training_output = np.dot(phi_matrix_training_data, np.transpose(weight_array))
    print("Computed training_output.")
    print("Computing validation_output...")
    validation_output = np.dot(phi_matrix_validation_data, np.transpose(weight_array))
    print("Computed validation_output.")
    print("Computing testing_output...")
    testing_output = np.dot(phi_matrix_testing_data, np.transpose(weight_array))
    print("Computed testing_output.")
    print("M = {0}, Lambda  = {1} Learning rate={2}".format(NumberOfClusters, lambda_term, learning_rate))
    print("Computing RMS error and accuracy for training data...")
    training_rms_error, training_accuracy = compute_rms_error_and_accuracy(training_output, training_target)
    print("RMS error: {0}. Accuracy:{1}".format(training_rms_error, training_accuracy))
    print("Computing RMS error and accuracy for validation data...")
    validation_rms_error, validation_accuracy = compute_rms_error_and_accuracy(validation_output, validation_target)
    print("RMS error: {0}. Accuracy:{1}".format(validation_rms_error, validation_accuracy))
    print("Computing RMS error and accuracy for testing data...")
    testing_rms_error, testing_accuracy = compute_rms_error_and_accuracy(testing_output, testing_target)
    print("RMS error: {0}. Accuracy:{1}".format(testing_rms_error, testing_accuracy))


############################ MAIN METHODS BEING CALLED ##############################
generate_closed_form_solution()
generate_sgd_solution(TrainingData, TrainingTarget, ValData, ValDataAct, TestData, TestDataAct, NumberOfFeatures,
                      IsSynthetic)


################################ METHODS FOR GRAPH GENERATION ############################


def generate_sgd_solutions_for_different_cluster_sizes(training_input, training_target, validation_input,
                                                       validation_target,
                                                       testing_input, testing_target, number_of_features, is_synthetic):
    learning_rate = 1
    lambda_term = 2
    cluster_choices = [10, 20, 50, 100]
    e_rms_training_values_for_cluster_choices = np.zeros((len(cluster_choices)))
    e_rms_validation_values_for_cluster_choices = np.zeros((len(cluster_choices)))
    e_rms_testing_values_for_cluster_choices = np.zeros((len(cluster_choices)))
    accuracy_training_values_for_cluster_choices = np.zeros((len(cluster_choices)))
    accuracy_validation_values_for_cluster_choices = np.zeros((len(cluster_choices)))
    accuracy_testing_values_for_cluster_choices = np.zeros((len(cluster_choices)))
    for iteration in range(len(cluster_choices)):
        print("\n\n-------NUMBER OF CLUSTERS: {0}--------".format(cluster_choices[iteration]))
        weight_array = np.ones((1, cluster_choices[iteration]))
        print("Computing cluster of size: {0}".format(cluster_choices[iteration]))
        kMeans = KMeans(n_clusters=cluster_choices[iteration], random_state=0).fit(TrainingData)
        mu_matrix = kMeans.cluster_centers_
        print("Computed cluster of size: {0}".format(cluster_choices[iteration]))
        big_sigma_matrix = generate_big_sigma_matrix(training_input, number_of_features, is_synthetic)
        big_sigma_inverse = np.linalg.inv(big_sigma_matrix)
        phi_matrix_training_data = generate_phi_matrix_for_inputs(training_input, mu_matrix, big_sigma_inverse)
        phi_matrix_validation_data = generate_phi_matrix_for_inputs(validation_input, mu_matrix, big_sigma_inverse)
        phi_matrix_testing_data = generate_phi_matrix_for_inputs(testing_input, mu_matrix, big_sigma_inverse)
        start_index = 0
        for i in range(NUMBER_OF_ITERATIONS):
            start_index = start_index + BATCH_SIZE
            end_index = start_index + BATCH_SIZE
            if end_index > len(training_input):
                start_index = 0
                end_index = BATCH_SIZE

            print("ITERATION: {0}, START_INDEX_OF_BATCH:{1}, END_INDEX_OF_BATCH:{2}".format(i, start_index, end_index))
            weight_array = update_weights(weight_array,
                                          compute_gradient(phi_matrix_training_data, start_index, end_index,
                                                           TrainingTarget,
                                                           weight_array, cluster_choices[iteration], lambda_term),
                                          learning_rate)
        print("Weight array: {0}".format(weight_array))
        print("Computed weight_matrix.")
        print("Computing training_output...")
        training_output = np.dot(phi_matrix_training_data, np.transpose(weight_array))
        print("Computed training_output.")
        print("Computing validation_output...")
        validation_output = np.dot(phi_matrix_validation_data, np.transpose(weight_array))
        print("Computed validation_output.")
        print("Computing testing_output...")
        testing_output = np.dot(phi_matrix_testing_data, np.transpose(weight_array))
        print("Computed testing_output.")

        print("Computing RMS error and accuracy for training data...")
        training_rms_error, training_accuracy = compute_rms_error_and_accuracy(training_output, training_target)
        print("RMS error: {0}. Accuracy:{1}".format(training_rms_error, training_accuracy))
        print("Computing RMS error and accuracy for validation data...")
        validation_rms_error, validation_accuracy = compute_rms_error_and_accuracy(validation_output, validation_target)
        print("RMS error: {0}. Accuracy:{1}".format(validation_rms_error, validation_accuracy))
        print("Computing RMS error and accuracy for testing data...")
        testing_rms_error, testing_accuracy = compute_rms_error_and_accuracy(testing_output, testing_target)
        e_rms_training_values_for_cluster_choices[iteration] = training_rms_error
        e_rms_validation_values_for_cluster_choices[iteration] = validation_rms_error
        e_rms_testing_values_for_cluster_choices[iteration] = testing_rms_error
        accuracy_training_values_for_cluster_choices[iteration] = training_accuracy
        accuracy_validation_values_for_cluster_choices[iteration] = validation_accuracy
        accuracy_testing_values_for_cluster_choices[iteration] = testing_accuracy

        print("RMS error: {0}. Accuracy:{1}".format(testing_rms_error, testing_accuracy))

    generate_figure_for_clusters(cluster_choices, e_rms_training_values_for_cluster_choices,
                                 e_rms_validation_values_for_cluster_choices,
                                 e_rms_testing_values_for_cluster_choices,
                                 accuracy_training_values_for_cluster_choices,
                                 accuracy_validation_values_for_cluster_choices,
                                 accuracy_testing_values_for_cluster_choices, "Cluster",
                                 "RMS error", "RMS error for different cluster sizes")


def generate_sgd_solutions_for_different_lambda_values(training_input, training_target, validation_input,
                                                       validation_target,
                                                       testing_input, testing_target, number_of_features, is_synthetic):
    learning_rate = 1
    number_of_clusters = 10
    lambda_choices = [0.001, 0.005, 0.01, 0.02, 0.03, 0.04, 0.05, 0.1, 1, 5, 10]
    weight_array = np.ones((1, number_of_clusters))
    print("Computing cluster of size: {0}".format(number_of_clusters))
    kMeans = KMeans(n_clusters=number_of_clusters, random_state=0).fit(TrainingData)
    mu_matrix = kMeans.cluster_centers_
    print("Computed cluster of size: {0}".format(number_of_clusters))
    big_sigma_matrix = generate_big_sigma_matrix(training_input, number_of_features, is_synthetic)
    big_sigma_inverse = np.linalg.inv(big_sigma_matrix)
    phi_matrix_training_data = generate_phi_matrix_for_inputs(training_input, mu_matrix, big_sigma_inverse)
    phi_matrix_validation_data = generate_phi_matrix_for_inputs(validation_input, mu_matrix, big_sigma_inverse)
    phi_matrix_testing_data = generate_phi_matrix_for_inputs(testing_input, mu_matrix, big_sigma_inverse)
    e_rms_training_values_for_lambda_choices = np.zeros((len(lambda_choices)))
    e_rms_validation_values_for_lambda_choices = np.zeros((len(lambda_choices)))
    e_rms_testing_values_for_lambda_choices = np.zeros((len(lambda_choices)))
    accuracy_training_values_for_lambda_choices = np.zeros((len(lambda_choices)))
    accuracy_validation_values_for_lambda_choices = np.zeros((len(lambda_choices)))
    accuracy_testing_values_for_lambda_choices = np.zeros((len(lambda_choices)))
    for iteration in range(len(lambda_choices)):
        print("\n\n-------NUMBER OF CLUSTERS: {0}--------".format(number_of_clusters))
        start_index = 0
        for i in range(NUMBER_OF_ITERATIONS):
            start_index = start_index + BATCH_SIZE
            end_index = start_index + BATCH_SIZE
            if end_index > len(training_input):
                start_index = 0
                end_index = BATCH_SIZE

            print("ITERATION: {0}, START_INDEX_OF_BATCH:{1}, END_INDEX_OF_BATCH:{2}".format(i, start_index, end_index))
            weight_array = update_weights(weight_array,
                                          compute_gradient(phi_matrix_training_data, start_index, end_index,
                                                           TrainingTarget,
                                                           weight_array, number_of_clusters, lambda_choices[iteration]),
                                          learning_rate)
        print("Weight array: {0}".format(weight_array))
        print("Computed weight_matrix.")
        print("Computing training_output...")
        training_output = np.dot(phi_matrix_training_data, np.transpose(weight_array))
        print("Computed training_output.")
        print("Computing validation_output...")
        validation_output = np.dot(phi_matrix_validation_data, np.transpose(weight_array))
        print("Computed validation_output.")
        print("Computing testing_output...")
        testing_output = np.dot(phi_matrix_testing_data, np.transpose(weight_array))
        print("Computed testing_output.")

        print("Computing RMS error and accuracy for training data...")
        training_rms_error, training_accuracy = compute_rms_error_and_accuracy(training_output, training_target)
        print("RMS error: {0}. Accuracy:{1}".format(training_rms_error, training_accuracy))
        print("Computing RMS error and accuracy for validation data...")
        validation_rms_error, validation_accuracy = compute_rms_error_and_accuracy(validation_output, validation_target)
        print("RMS error: {0}. Accuracy:{1}".format(validation_rms_error, validation_accuracy))
        print("Computing RMS error and accuracy for testing data...")
        testing_rms_error, testing_accuracy = compute_rms_error_and_accuracy(testing_output, testing_target)
        e_rms_training_values_for_lambda_choices[iteration] = training_rms_error
        e_rms_validation_values_for_lambda_choices[iteration] = validation_rms_error
        e_rms_testing_values_for_lambda_choices[iteration] = testing_rms_error
        accuracy_training_values_for_lambda_choices[iteration] = training_accuracy
        accuracy_validation_values_for_lambda_choices[iteration] = validation_accuracy
        accuracy_testing_values_for_lambda_choices[iteration] = testing_accuracy

        print("RMS error: {0}. Accuracy:{1}".format(testing_rms_error, testing_accuracy))

    generate_figure_for_clusters(lambda_choices, e_rms_training_values_for_lambda_choices,
                                 e_rms_validation_values_for_lambda_choices,
                                 e_rms_testing_values_for_lambda_choices,
                                 accuracy_training_values_for_lambda_choices,
                                 accuracy_validation_values_for_lambda_choices,
                                 accuracy_testing_values_for_lambda_choices, "Lambda",
                                 "RMS error", "RMS error for different cluster sizes")


def generate_sgd_solutions_for_different_learning_rates(training_input, training_target, validation_input,
                                                        validation_target,
                                                        testing_input, testing_target, number_of_features,
                                                        is_synthetic):
    lambda_term = 2
    number_of_clusters = 10
    learning_rate_choices = [0.001, 0.005, 0.01, 0.02, 0.03, 0.04, 0.05, 0.1, 0.5]
    weight_array = np.ones((1, number_of_clusters))
    print("Computing cluster of size: {0}".format(number_of_clusters))
    kMeans = KMeans(n_clusters=number_of_clusters, random_state=0).fit(TrainingData)
    mu_matrix = kMeans.cluster_centers_
    print("Computed cluster of size: {0}".format(number_of_clusters))
    big_sigma_matrix = generate_big_sigma_matrix(training_input, number_of_features, is_synthetic)
    big_sigma_inverse = np.linalg.inv(big_sigma_matrix)
    phi_matrix_training_data = generate_phi_matrix_for_inputs(training_input, mu_matrix, big_sigma_inverse)
    phi_matrix_validation_data = generate_phi_matrix_for_inputs(validation_input, mu_matrix, big_sigma_inverse)
    phi_matrix_testing_data = generate_phi_matrix_for_inputs(testing_input, mu_matrix, big_sigma_inverse)
    e_rms_training_values_for_learning_rate_choices = np.zeros((len(learning_rate_choices)))
    e_rms_validation_values_for_learning_rate_choices = np.zeros((len(learning_rate_choices)))
    e_rms_testing_values_for_learning_rate_choices = np.zeros((len(learning_rate_choices)))
    accuracy_training_values_for_learning_rate_choices = np.zeros((len(learning_rate_choices)))
    accuracy_validation_values_for_learning_rate_choices = np.zeros((len(learning_rate_choices)))
    accuracy_testing_values_for_learning_rate_choices = np.zeros((len(learning_rate_choices)))
    for iteration in range(len(learning_rate_choices)):
        print("\n\n-------NUMBER OF CLUSTERS: {0}--------".format(number_of_clusters))
        start_index = 0
        for i in range(NUMBER_OF_ITERATIONS):
            start_index = start_index + BATCH_SIZE
            end_index = start_index + BATCH_SIZE
            if end_index > len(training_input):
                start_index = 0
                end_index = BATCH_SIZE

            print("ITERATION: {0}, START_INDEX_OF_BATCH:{1}, END_INDEX_OF_BATCH:{2}".format(i, start_index, end_index))
            weight_array = update_weights(weight_array,
                                          compute_gradient(phi_matrix_training_data, start_index, end_index,
                                                           TrainingTarget,
                                                           weight_array, number_of_clusters, lambda_term),
                                          learning_rate_choices[iteration])
        print("Weight array: {0}".format(weight_array))
        print("Computed weight_matrix.")
        print("Computing training_output...")
        training_output = np.dot(phi_matrix_training_data, np.transpose(weight_array))
        print("Computed training_output.")
        print("Computing validation_output...")
        validation_output = np.dot(phi_matrix_validation_data, np.transpose(weight_array))
        print("Computed validation_output.")
        print("Computing testing_output...")
        testing_output = np.dot(phi_matrix_testing_data, np.transpose(weight_array))
        print("Computed testing_output.")

        print("Computing RMS error and accuracy for training data...")
        training_rms_error, training_accuracy = compute_rms_error_and_accuracy(training_output, training_target)
        print("RMS error: {0}. Accuracy:{1}".format(training_rms_error, training_accuracy))
        print("Computing RMS error and accuracy for validation data...")
        validation_rms_error, validation_accuracy = compute_rms_error_and_accuracy(validation_output, validation_target)
        print("RMS error: {0}. Accuracy:{1}".format(validation_rms_error, validation_accuracy))
        print("Computing RMS error and accuracy for testing data...")
        testing_rms_error, testing_accuracy = compute_rms_error_and_accuracy(testing_output, testing_target)
        e_rms_training_values_for_learning_rate_choices[iteration] = training_rms_error
        e_rms_validation_values_for_learning_rate_choices[iteration] = validation_rms_error
        e_rms_testing_values_for_learning_rate_choices[iteration] = testing_rms_error
        accuracy_training_values_for_learning_rate_choices[iteration] = training_accuracy
        accuracy_validation_values_for_learning_rate_choices[iteration] = validation_accuracy
        accuracy_testing_values_for_learning_rate_choices[iteration] = testing_accuracy

        print("RMS error: {0}. Accuracy:{1}".format(testing_rms_error, testing_accuracy))

    generate_figure_for_clusters(learning_rate_choices, e_rms_training_values_for_learning_rate_choices,
                                 e_rms_validation_values_for_learning_rate_choices,
                                 e_rms_testing_values_for_learning_rate_choices,
                                 accuracy_training_values_for_learning_rate_choices,
                                 accuracy_validation_values_for_learning_rate_choices,
                                 accuracy_testing_values_for_learning_rate_choices, "Learning rate",
                                 "RMS error", "RMS error for different learning rates")


def generate_sgd_solutions_for_different_variances(training_input, training_target, validation_input,
                                                   validation_target,
                                                   testing_input, testing_target, number_of_features,
                                                   is_synthetic):
    learning_rate = 1
    lambda_term = 2
    number_of_clusters = 10
    variance_choices = [0.001, 0.01, 0.1, 0.2, 0.5, 1, 2]
    weight_array = np.ones((1, number_of_clusters))
    print("Computing cluster of size: {0}".format(number_of_clusters))
    kMeans = KMeans(n_clusters=number_of_clusters, random_state=0).fit(TrainingData)
    mu_matrix = kMeans.cluster_centers_
    print("Computed cluster of size: {0}".format(number_of_clusters))

    e_rms_training_values_for_variance_choices = np.zeros((len(variance_choices)))
    e_rms_validation_values_for_variance_choices = np.zeros((len(variance_choices)))
    e_rms_testing_values_for_variance_choices = np.zeros((len(variance_choices)))
    accuracy_training_values_for_variance_choices = np.zeros((len(variance_choices)))
    accuracy_validation_values_for_variance_choices = np.zeros((len(variance_choices)))
    accuracy_testing_values_for_variance_choices = np.zeros((len(variance_choices)))
    for iteration in range(len(variance_choices)):
        print("\n\n-------NUMBER OF CLUSTERS: {0}--------".format(number_of_clusters))
        start_index = 0
        big_sigma_matrix = generate_different_big_sigma_matrix(training_input, number_of_features,
                                                               variance_choices[iteration],
                                                               is_synthetic)
        big_sigma_inverse = np.linalg.inv(big_sigma_matrix)
        phi_matrix_training_data = generate_phi_matrix_for_inputs(training_input, mu_matrix, big_sigma_inverse)
        phi_matrix_validation_data = generate_phi_matrix_for_inputs(validation_input, mu_matrix, big_sigma_inverse)
        phi_matrix_testing_data = generate_phi_matrix_for_inputs(testing_input, mu_matrix, big_sigma_inverse)
        for i in range(NUMBER_OF_ITERATIONS):
            start_index = start_index + BATCH_SIZE
            end_index = start_index + BATCH_SIZE
            if end_index > len(training_input):
                start_index = 0
                end_index = BATCH_SIZE

            print("ITERATION: {0}, START_INDEX_OF_BATCH:{1}, END_INDEX_OF_BATCH:{2}".format(i, start_index,
                                                                                            end_index))
            weight_array = update_weights(weight_array,
                                          compute_gradient(phi_matrix_training_data, start_index, end_index,
                                                           TrainingTarget, weight_array, number_of_clusters,
                                                           lambda_term), learning_rate)
        print("Weight array: {0}".format(weight_array))
        print("Computed weight_matrix.")
        print("Computing training_output...")
        training_output = np.dot(phi_matrix_training_data, np.transpose(weight_array))
        print("Computed training_output.")
        print("Computing validation_output...")
        validation_output = np.dot(phi_matrix_validation_data, np.transpose(weight_array))
        print("Computed validation_output.")
        print("Computing testing_output...")
        testing_output = np.dot(phi_matrix_testing_data, np.transpose(weight_array))
        print("Computed testing_output.")

        print("Computing RMS error and accuracy for training data...")
        training_rms_error, training_accuracy = compute_rms_error_and_accuracy(training_output, training_target)
        print("RMS error: {0}. Accuracy:{1}".format(training_rms_error, training_accuracy))
        print("Computing RMS error and accuracy for validation data...")
        validation_rms_error, validation_accuracy = compute_rms_error_and_accuracy(validation_output,
                                                                                   validation_target)
        print("RMS error: {0}. Accuracy:{1}".format(validation_rms_error, validation_accuracy))
        print("Computing RMS error and accuracy for testing data...")
        testing_rms_error, testing_accuracy = compute_rms_error_and_accuracy(testing_output, testing_target)
        e_rms_training_values_for_variance_choices[iteration] = training_rms_error
        e_rms_validation_values_for_variance_choices[iteration] = validation_rms_error
        e_rms_testing_values_for_variance_choices[iteration] = testing_rms_error
        accuracy_training_values_for_variance_choices[iteration] = training_accuracy
        accuracy_validation_values_for_variance_choices[iteration] = validation_accuracy
        accuracy_testing_values_for_variance_choices[iteration] = testing_accuracy

        print("RMS error: {0}. Accuracy:{1}".format(testing_rms_error, testing_accuracy))

    generate_figure_for_clusters(variance_choices, e_rms_training_values_for_variance_choices,
                                 e_rms_validation_values_for_variance_choices,
                                 e_rms_testing_values_for_variance_choices,
                                 accuracy_training_values_for_variance_choices,
                                 accuracy_validation_values_for_variance_choices,
                                 accuracy_testing_values_for_variance_choices, "Variance multiplier",
                                 "RMS error", "RMS error for different learning rates")


def closed_form_for_different_cluster_choices(training_input, training_target, validation_input, validation_target,
                                              testing_input, testing_target, number_of_features, lambda_value,
                                              is_synthetic, include_bias):
    big_sigma_matrix = generate_big_sigma_matrix(training_input, number_of_features, is_synthetic)
    big_sigma_inverse = np.linalg.inv(big_sigma_matrix)
    cluster_choices = [1, 3, 5, 8, 10, 20, 40, 60, 100]
    e_rms_training_values_for_cluster_choices = np.zeros((len(cluster_choices)))
    e_rms_validation_values_for_cluster_choices = np.zeros((len(cluster_choices)))
    e_rms_testing_values_for_cluster_choices = np.zeros((len(cluster_choices)))
    accuracy_training_values_for_cluster_choices = np.zeros((len(cluster_choices)))
    accuracy_validation_values_for_cluster_choices = np.zeros((len(cluster_choices)))
    accuracy_testing_values_for_cluster_choices = np.zeros((len(cluster_choices)))
    for iteration in range(len(cluster_choices)):
        print("\n\n-------NUMBER OF CLUSTERS: {0}--------".format(cluster_choices[iteration]))

        print("Computing clusters for number of clusters: {0}".format(cluster_choices[iteration]))
        # We divide the input feature space into "NumberOfClusters" clusters. With "NumberOfFeatures" features,
        # we end up with a matrix of shape ("NumberOfClusters", "NumberOfFeatures").
        # This means that we now have  with "NumberOfClusters" centroid values for the "NumberOfClusters" clusters
        # of the "NumberOfFeatures" features.
        k_means = KMeans(n_clusters=cluster_choices[iteration], random_state=0).fit(TrainingData)
        mu_matrix = k_means.cluster_centers_
        print("Computed clusters for number of clusters: {0}".format(cluster_choices[iteration]))
        print("Computing phi_matrix_training_data...")
        phi_matrix_training_data = generate_phi_matrix_for_inputs(training_input, mu_matrix, big_sigma_inverse)
        print("Computed phi_matrix_training_data.")
        print("Computing phi_matrix_validation_data...")
        phi_matrix_validation_data = generate_phi_matrix_for_inputs(validation_input, mu_matrix, big_sigma_inverse)
        print("Computed phi_matrix_validation_data.")
        print("Computing phi_matrix_testing_data...")
        phi_matrix_testing_data = generate_phi_matrix_for_inputs(testing_input, mu_matrix, big_sigma_inverse)
        print("Computed phi_matrix_testing_data.")
        lambda_diagonal_matrix = np.diag(np.diag(np.full((cluster_choices[iteration], cluster_choices[iteration]),
                                                         lambda_value)))
        if include_bias:
            training_input_phi_0 = np.ones(len(training_input))[..., None]
            validation_input_phi_0 = np.ones(len(validation_input))[..., None]
            testing_input_phi_0 = np.ones(len(testing_input))[..., None]
            print(np.shape(training_input_phi_0))
            phi_matrix_training_data = np.append(training_input_phi_0, phi_matrix_training_data, 1)
            phi_matrix_validation_data = np.append(validation_input_phi_0, phi_matrix_validation_data, 1)
            phi_matrix_testing_data = np.append(testing_input_phi_0, phi_matrix_testing_data, 1)
            lambda_diagonal_matrix = np.diag(np.diag(np.full((cluster_choices[iteration] + 1,
                                                              cluster_choices[iteration] + 1), lambda_value)))

        print("Computing weight_matrix...")
        weight_matrix = compute_weight_matrix(training_target, phi_matrix_training_data,
                                              np.transpose(phi_matrix_training_data), lambda_diagonal_matrix)
        print("Computed weight_matrix.")
        print("Computing training_output...")
        training_output = compute_output(weight_matrix, phi_matrix_training_data)
        print("Computed training_output.")
        print("Computing validation_output...")
        validation_output = compute_output(weight_matrix, phi_matrix_validation_data)
        print("Computed validation_output.")
        print("Computing testing_output...")
        testing_output = compute_output(weight_matrix, phi_matrix_testing_data)
        print("Computed testing_output.")

        print("Computing RMS error and accuracy for training data...")
        training_rms_error, training_accuracy = compute_rms_error_and_accuracy(training_output, training_target)
        print("Computing RMS error and accuracy for validation data...")
        validation_rms_error, validation_accuracy = compute_rms_error_and_accuracy(validation_output, validation_target)
        print("Computing RMS error and accuracy for testing data...")
        testing_rms_error, testing_accuracy = compute_rms_error_and_accuracy(testing_output, testing_target)
        e_rms_training_values_for_cluster_choices[iteration] = training_rms_error
        e_rms_validation_values_for_cluster_choices[iteration] = validation_rms_error
        e_rms_testing_values_for_cluster_choices[iteration] = testing_rms_error
        accuracy_training_values_for_cluster_choices[iteration] = training_accuracy
        accuracy_validation_values_for_cluster_choices[iteration] = validation_accuracy
        accuracy_testing_values_for_cluster_choices[iteration] = testing_accuracy

    generate_figure_for_clusters(cluster_choices, e_rms_training_values_for_cluster_choices,
                                 e_rms_validation_values_for_cluster_choices, e_rms_testing_values_for_cluster_choices,
                                 accuracy_training_values_for_cluster_choices,
                                 accuracy_validation_values_for_cluster_choices,
                                 accuracy_testing_values_for_cluster_choices, "Number of clusters",
                                 "RMS error", "RMS error for different cluster sizes")


def closed_form_for_different_lambda_choices(training_input, training_target, validation_input, validation_target,
                                             testing_input, testing_target, number_of_features, cluster_value,
                                             is_synthetic, include_bias):
    big_sigma_matrix = generate_big_sigma_matrix(training_input, number_of_features, is_synthetic)
    big_sigma_inverse = np.linalg.inv(big_sigma_matrix)
    lambda_choices = [0.001, 0.005, 0.01, 0.02, 0.03, 0.04, 0.05, 0.1]
    # We divide the input feature space into "NumberOfClusters" clusters. With "NumberOfFeatures" features,
    # we end up with a matrix of shape ("NumberOfClusters", "NumberOfFeatures").
    # This means that we now have  with "NumberOfClusters" centroid values for the "NumberOfClusters" clusters
    # of the "NumberOfFeatures" features.
    print("Computing clusters")
    k_means = KMeans(n_clusters=cluster_value, random_state=0).fit(TrainingData)
    mu_matrix = k_means.cluster_centers_
    print("Computed clusters")
    print("Computing phi_matrix_training_data...")
    phi_matrix_training_data = generate_phi_matrix_for_inputs(training_input, mu_matrix, big_sigma_inverse)
    print("Computed phi_matrix_training_data.")
    print("Computing phi_matrix_validation_data...")
    phi_matrix_validation_data = generate_phi_matrix_for_inputs(validation_input, mu_matrix, big_sigma_inverse)
    print("Computed phi_matrix_validation_data.")
    print("Computing phi_matrix_testing_data...")
    phi_matrix_testing_data = generate_phi_matrix_for_inputs(testing_input, mu_matrix, big_sigma_inverse)
    print("Computed phi_matrix_testing_data.")
    e_rms_training_values_for_lambda_choices = np.zeros((len(lambda_choices)))
    e_rms_validation_values_for_lambda_choices = np.zeros((len(lambda_choices)))
    e_rms_testing_values_for_lambda_choices = np.zeros((len(lambda_choices)))
    accuracy_training_values_for_lambda_choices = np.zeros((len(lambda_choices)))
    accuracy_validation_values_for_lambda_choices = np.zeros((len(lambda_choices)))
    accuracy_testing_values_for_lambda_choices = np.zeros((len(lambda_choices)))
    for iteration in range(len(lambda_choices)):
        print("\n\n-------LAMBDA: {0}--------".format(lambda_choices[iteration]))
        lambda_diagonal_matrix = np.diag(np.diag(np.full((cluster_value, cluster_value),
                                                         lambda_choices[iteration])))
        # if include_bias:
        #     training_input_phi_0 = np.ones(len(training_input))[..., None]
        #     validation_input_phi_0 = np.ones(len(validation_input))[..., None]
        #     testing_input_phi_0 = np.ones(len(testing_input))[..., None]
        #     print(np.shape(training_input_phi_0))
        #     phi_matrix_training_data = np.append(training_input_phi_0, phi_matrix_training_data, 1)
        #     phi_matrix_validation_data = np.append(validation_input_phi_0, phi_matrix_validation_data, 1)
        #     phi_matrix_testing_data = np.append(testing_input_phi_0, phi_matrix_testing_data, 1)
        #     lambda_diagonal_matrix = np.diag(np.diag(np.full((cluster_value + 1, cluster_value + 1),
        #                                                      lambda_choices[iteration])))

        print("Computing weight_matrix...")
        weight_matrix = compute_weight_matrix(training_target, phi_matrix_training_data,
                                              np.transpose(phi_matrix_training_data), lambda_diagonal_matrix)
        print("Computed weight_matrix.")
        print("Computing training_output...")
        training_output = compute_output(weight_matrix, phi_matrix_training_data)
        print("Computed training_output.")
        print("Computing validation_output...")
        validation_output = compute_output(weight_matrix, phi_matrix_validation_data)
        print("Computed validation_output.")
        print("Computing testing_output...")
        testing_output = compute_output(weight_matrix, phi_matrix_testing_data)
        print("Computed testing_output.")

        print("Computing RMS error and accuracy for training data...")
        training_rms_error, training_accuracy = compute_rms_error_and_accuracy(training_output, training_target)
        print("Computing RMS error and accuracy for validation data...")
        validation_rms_error, validation_accuracy = compute_rms_error_and_accuracy(validation_output,
                                                                                   validation_target)
        print("Computing RMS error and accuracy for testing data...")
        testing_rms_error, testing_accuracy = compute_rms_error_and_accuracy(testing_output, testing_target)
        e_rms_training_values_for_lambda_choices[iteration] = training_rms_error
        e_rms_validation_values_for_lambda_choices[iteration] = validation_rms_error
        e_rms_testing_values_for_lambda_choices[iteration] = testing_rms_error
        accuracy_training_values_for_lambda_choices[iteration] = training_accuracy
        accuracy_validation_values_for_lambda_choices[iteration] = validation_accuracy
        accuracy_testing_values_for_lambda_choices[iteration] = testing_accuracy

    generate_figure_for_clusters(lambda_choices, e_rms_training_values_for_lambda_choices,
                                 e_rms_validation_values_for_lambda_choices,
                                 e_rms_testing_values_for_lambda_choices,
                                 accuracy_training_values_for_lambda_choices,
                                 accuracy_validation_values_for_lambda_choices,
                                 accuracy_testing_values_for_lambda_choices, "Lambda",
                                 "RMS error", "RMS error for different cluster sizes")


def closed_form_for_different_variance_choices(training_input, training_target, validation_input, validation_target,
                                               testing_input, testing_target, number_of_features, cluster_value,
                                               lambda_value, is_synthetic, include_bias):
    variance_choices = [0.001, 0.01, 0.1, 0.2, 0.5, 1, 2]
    # We divide the input feature space into "NumberOfClusters" clusters. With "NumberOfFeatures" features,
    # we end up with a matrix of shape ("NumberOfClusters", "NumberOfFeatures").
    # This means that we now have  with "NumberOfClusters" centroid values for the "NumberOfClusters" clusters
    # of the "NumberOfFeatures" features.
    print("Computing clusters")
    k_means = KMeans(n_clusters=cluster_value, random_state=0).fit(TrainingData)
    mu_matrix = k_means.cluster_centers_

    lambda_diagonal_matrix = np.diag(np.diag(np.full((cluster_value, cluster_value),
                                                     lambda_value)))
    e_rms_training_values_for_variance_choices = np.zeros((len(variance_choices)))
    e_rms_validation_values_for_variance_choices = np.zeros((len(variance_choices)))
    e_rms_testing_values_for_variance_choices = np.zeros((len(variance_choices)))
    accuracy_training_values_for_variance_choices = np.zeros((len(variance_choices)))
    accuracy_validation_values_for_variance_choices = np.zeros((len(variance_choices)))
    accuracy_testing_values_for_variance_choices = np.zeros((len(variance_choices)))
    for iteration in range(len(variance_choices)):
        print("\n\n-------VARIANCE: {0}--------".format(variance_choices[iteration]))
        big_sigma_matrix = generate_different_big_sigma_matrix(training_input, number_of_features,
                                                               variance_choices[iteration], is_synthetic)
        big_sigma_inverse = np.linalg.inv(big_sigma_matrix)
        # if include_bias:
        #     training_input_phi_0 = np.ones(len(training_input))[..., None]
        #     validation_input_phi_0 = np.ones(len(validation_input))[..., None]
        #     testing_input_phi_0 = np.ones(len(testing_input))[..., None]
        #     print(np.shape(training_input_phi_0))
        #     phi_matrix_training_data = np.append(training_input_phi_0, phi_matrix_training_data, 1)
        #     phi_matrix_validation_data = np.append(validation_input_phi_0, phi_matrix_validation_data, 1)
        #     phi_matrix_testing_data = np.append(testing_input_phi_0, phi_matrix_testing_data, 1)
        #     lambda_diagonal_matrix = np.diag(np.diag(np.full((cluster_value + 1, cluster_value + 1),
        #                                                      lambda_choices[iteration])))
        print("Computed clusters")
        print("Computing phi_matrix_training_data...")
        phi_matrix_training_data = generate_phi_matrix_for_inputs(training_input, mu_matrix, big_sigma_inverse)
        print("Computed phi_matrix_training_data.")
        print("Computing phi_matrix_validation_data...")
        phi_matrix_validation_data = generate_phi_matrix_for_inputs(validation_input, mu_matrix, big_sigma_inverse)
        print("Computed phi_matrix_validation_data.")
        print("Computing phi_matrix_testing_data...")
        phi_matrix_testing_data = generate_phi_matrix_for_inputs(testing_input, mu_matrix, big_sigma_inverse)
        print("Computed phi_matrix_testing_data.")
        print("Computing weight_matrix...")
        weight_matrix = compute_weight_matrix(training_target, phi_matrix_training_data,
                                              np.transpose(phi_matrix_training_data), lambda_diagonal_matrix)
        print("Computed weight_matrix.")
        print("Computing training_output...")
        training_output = compute_output(weight_matrix, phi_matrix_training_data)
        print("Computed training_output.")
        print("Computing validation_output...")
        validation_output = compute_output(weight_matrix, phi_matrix_validation_data)
        print("Computed validation_output.")
        print("Computing testing_output...")
        testing_output = compute_output(weight_matrix, phi_matrix_testing_data)
        print("Computed testing_output.")

        print("Computing RMS error and accuracy for training data...")
        training_rms_error, training_accuracy = compute_rms_error_and_accuracy(training_output, training_target)
        print("Computing RMS error and accuracy for validation data...")
        validation_rms_error, validation_accuracy = compute_rms_error_and_accuracy(validation_output,
                                                                                   validation_target)
        print("Computing RMS error and accuracy for testing data...")
        testing_rms_error, testing_accuracy = compute_rms_error_and_accuracy(testing_output, testing_target)
        e_rms_training_values_for_variance_choices[iteration] = training_rms_error
        e_rms_validation_values_for_variance_choices[iteration] = validation_rms_error
        e_rms_testing_values_for_variance_choices[iteration] = testing_rms_error
        accuracy_training_values_for_variance_choices[iteration] = training_accuracy
        accuracy_validation_values_for_variance_choices[iteration] = validation_accuracy
        accuracy_testing_values_for_variance_choices[iteration] = testing_accuracy

    generate_figure_for_clusters(variance_choices, e_rms_training_values_for_variance_choices,
                                 e_rms_validation_values_for_variance_choices,
                                 e_rms_testing_values_for_variance_choices,
                                 accuracy_training_values_for_variance_choices,
                                 accuracy_validation_values_for_variance_choices,
                                 accuracy_testing_values_for_variance_choices, "Variance",
                                 "RMS error", "RMS error for different cluster sizes")


def generate_figure_for_clusters(x_axis_data,
                                 training_rms_data, validation_rms_data, testing_rms_data,
                                 training_accuracy_data, validation_accuracy_data, testing_accuracy_data,
                                 x_axis_label, y_axis_label, title):
    fig = plt.figure()
    plt.figure(1)
    plt.plot(x_axis_data, training_rms_data, 'r--', label="Training")
    plt.plot(x_axis_data, validation_rms_data, 'g--', label="Validation")
    plt.plot(x_axis_data, testing_rms_data, 'b--', label="Testing")
    plt.xlabel(x_axis_label)
    plt.ylabel("RMS error")
    plt.legend(loc='upper left')
    plt.figure(2)
    plt.plot(x_axis_data, training_accuracy_data, 'r-', label="Training")
    plt.plot(x_axis_data, validation_accuracy_data, 'g-', label="Validation")
    plt.plot(x_axis_data, testing_accuracy_data, 'b-', label="Testing")
    plt.xlabel(x_axis_label)
    plt.ylabel("Accuracy")
    plt.legend(loc='upper left')
    plt.show()


def generate_cluster_choice_graph(training_input):
    distorsions = []
    for k in range(1, 100, 10):
        kmeans = KMeans(n_clusters=k)
        kmeans.fit(training_input)
        print("Computed clusters for number of clusters: {0}".format(k))
        distorsions.append(kmeans.inertia_)
    print("Plotting now")
    fig = plt.figure(figsize=(15, 5))
    plt.plot(range(1, 10), distorsions)
    plt.grid(True)
    plt.title('Elbow curve')
    plt.show()


def generate_different_big_sigma_matrix(data, number_of_features, multiply, is_synthetic):
    big_sigma_matrix = np.zeros(shape=(number_of_features, number_of_features))
    variance_matrix = np.var(data, axis=0)
    for feature in range(number_of_features):
        big_sigma_matrix[feature][feature] = variance_matrix[feature] * multiply
    if is_synthetic:
        big_sigma_matrix = np.dot(3, big_sigma_matrix)
    else:
        big_sigma_matrix = np.dot(200, big_sigma_matrix)
    return big_sigma_matrix
